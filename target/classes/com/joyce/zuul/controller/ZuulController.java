package com.joyce.zuul.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ZuulController {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ZuulController.class);
	
	@Value("${server.port}")
	private String serverPort;
	@Value("${spring.application.name}")
	private String spingApplicationName;
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping(value="/zuul/index")
	public String index(String name) {
		LOG.info("request /index param name=="+name);
		LOG.info("name==="+name);
		LOG.info("serverPort==="+serverPort);
		LOG.info("spingApplicationName==="+spingApplicationName);
		String result = "hello " + name+", " +spingApplicationName+":"+serverPort + ". ";
		return getCurrentTime() + result; //打上时间戳，方便观察结果
	}
	
	private String getCurrentTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return sdf.format(new Date())+"   ";
	}
}
