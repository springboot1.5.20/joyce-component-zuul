package com.joyce.zuul;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class ComponentZuulApplication {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ComponentZuulApplication.class);

	@Bean
	@LoadBalanced  //负载均衡，默认为轮询策略
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ComponentZuulApplication.class, args); 
		LOG.info("Hello World!");
	}
}
